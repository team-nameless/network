#pragma once

#include "t_PacketDump.h"
#include "Types.h"
#include "Packet.h"

namespace noname_packet_dump
{
	class Dump_manager
	{
	public:
		Dump_manager();
		Dump_manager(const char* dev_name);

		void connect_to_pcap(const char* dev);
		pcap_t* get_handle() const;
		int get_next_packet(Packet* next_packet);

		enum class STATUS
		{
			ONLINE,
			OFFLINE
		};

		STATUS status;

	private:
		char errbuf[PCAP_ERRBUF_SIZE];
		pcap_t* handle;
	};

}