#include "tcp.h"

namespace noname_packet_dump
{
	tcp_header::tcp_header()
		: src_port(0)
		, des_port(0)
		, seq_num(0)
		, ack_num(0)
		, header_len_flags(0)
		, window(0)
		, check_sum(0)
		, urgent_ptr(0) { }

	tcp_header::tcp_header(const uint8_t* data)
		: src_port(data[0])
		, des_port(data[2])
		, seq_num(data[4])
		, ack_num(data[8])
		, header_len_flags(data[12])
		, window(data[14])
		, check_sum(data[16])
		, urgent_ptr(data[18]) { }

	tcp_header::tcp_header(const tcp_header& t)
		: src_port(t.src_port)
		, des_port(t.des_port)
		, seq_num(t.seq_num)
		, ack_num(t.ack_num)
		, header_len_flags(t.header_len_flags)
		, window(t.window)
		, check_sum(t.check_sum)
		, urgent_ptr(t.urgent_ptr) { }

	std::string tcp_header::to_string() const
	{
		std::ostringstream ss;

		ss << "src port: " << ntohl(src_port) << std::endl
			<< "des port: " << ntohl(des_port) << std::endl;
		return ss.str();
	}

	tcp_header& tcp_header::operator=(const tcp_header& t)
	{
		src_port = t.src_port;
		des_port = t.des_port;
		seq_num = t.seq_num;
		ack_num = t.ack_num;
		header_len_flags = t.header_len_flags;
		window = t.window;
		check_sum = t.check_sum;
		urgent_ptr = t.urgent_ptr;
		return *this;
	}

	bool tcp_header::operator==(const tcp_header& t)
	{
		return src_port == t.src_port
			&& des_port == t.des_port
			&& seq_num == t.seq_num
			&& ack_num == t.ack_num
			&& header_len_flags == t.header_len_flags
			&& window == t.window
			&& check_sum == t.check_sum
			&& urgent_ptr == t.urgent_ptr;
	}

	bool tcp_header::operator!=(const tcp_header& t)
	{
		return !(*this == t);
	}

	std::ostream& operator<<(std::ostream& os, const tcp_header& t)
	{
		os << t.to_string();
		return os;
	}

}