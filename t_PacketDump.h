#pragma once

#include <stdint.h>
#include <string.h>

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>

#ifdef _WIN32
#include <winsock2.h>
#include <Windows.h>

#include "pcap.h"

#pragma comment (lib, "ws2_32.lib")
#pragma comment (lib, "Lib/wpcap.lib")
#endif

#ifdef __linux__
#include <pcap.h>
#include <byteswap.h>
#endif
