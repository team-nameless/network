#pragma once
#include "t_PacketDump.h"
#include "Types.h"

namespace noname_packet_dump
{
#pragma pack(push, 1)
	struct mac_address final
	{
		static constexpr auto LEN = 6;

		uint8_t address[LEN] = { 0 };

		mac_address();
		mac_address(const uint8_t* data);
		mac_address(const mac_address& m);

		std::string to_string(const char delimiter) const;

		mac_address  operator+(const mac_address& m) = delete;
		mac_address  operator-(const mac_address& m) = delete;
		mac_address  operator*(const mac_address& m) = delete;
		mac_address  operator/(const mac_address& m) = delete;
		mac_address  operator%(const mac_address& m) = delete;
		mac_address& operator=(const mac_address& m);
		bool		 operator==(const mac_address& m);
		bool		 operator!=(const mac_address& m);

		friend std::ostream& operator<<(std::ostream& os, const mac_address& m);
	};

	struct ethernet_header final : public header
	{
		static constexpr auto ETHER_TYPE_IP		= 0x0800;
		static constexpr auto ETHER_TYPE_ARP	= 0x0806;

	private:
		mac_address destination;
		mac_address source;
		uint16_t	ether_type;

	public:
		ethernet_header();
		ethernet_header(const uint8_t* data);
		ethernet_header(const ethernet_header& e);

		void		set_destination(mac_address destination);
		void		set_source(mac_address source);
		void		set_ether_type(uint16_t ether_type);
		mac_address get_destination() const;
		mac_address get_source() const;
		uint16_t	get_ether_type() const;

		std::string to_string() const;
		PacketType	get_next_packet_type() const;

		ethernet_header  operator+(const ethernet_header& m) = delete;
		ethernet_header  operator-(const ethernet_header& m) = delete;
		ethernet_header  operator*(const ethernet_header& m) = delete;
		ethernet_header  operator/(const ethernet_header& m) = delete;
		ethernet_header  operator%(const ethernet_header& m) = delete;
		ethernet_header& operator=(const ethernet_header& e);
		bool			 operator==(const ethernet_header& m);
		bool			 operator!=(const ethernet_header& m);

		friend std::ostream& operator<<(std::ostream& os, const ethernet_header& e);
	};
#pragma pack(pop)
}