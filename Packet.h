#pragma once

#include "t_PacketDump.h"
#include "Types.h"

namespace noname_packet_dump
{
	class Packet
	{
	public:

		Packet();

		void init();
		std::string to_string() const;

		struct pcap_pkthdr* p_header;
		const u_char* packet;

	private:
		std::vector<header*> headers;
	};



}