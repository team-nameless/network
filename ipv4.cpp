#include "ipv4.h"

namespace noname_packet_dump
{
	ip_address::ip_address() : address{ 0 } { }

	ip_address::ip_address(const uint8_t* data) : address{ 0 }
	{
		for (int i = 0; i < LEN; ++i)
			address[i] = data[i];
	}

	ip_address::ip_address(const ip_address& i) : address{ 0 }
	{
		for (int j = 0; j < LEN; ++j)
			address[j] = i.address[j];
	}

	std::string ip_address::to_string() const
	{
		std::ostringstream ss;
		ss << address[0] << "."
			<< address[1] << "."
			<< address[2] << "."
			<< address[3];
		return ss.str();
	}

	ip_address& ip_address::operator=(const ip_address& i)
	{
		for (int j = 0; j < LEN; ++j)
			address[j] = i.address[j];
		return *this;
	}

	bool ip_address::operator==(const ip_address& i)
	{
		for (int j = 0; j < LEN; ++j)
			if (address[j] != i.address[j]) return false;
		return true;
	}

	bool ip_address::operator!=(const ip_address& i)
	{
		return !(*this == i);
	}

	std::ostream& operator<<(std::ostream& os, const ip_address& i)
	{
		os << i.to_string();
		return os;
	}

	ip_header::ip_header()
		: header_length_and_version(0)
		, ip_type_of_serivce(0)
		, ip_length(0)
		, ip_id(0)
		, ip_flag_offset(0)
		, ip_ttl(0)
		, ip_proto(0)
		, ip_check_sum(0)
		, src_ip_addr(), des_ip_addr() { }

	ip_header::ip_header(const uint8_t* data)
		: header_length_and_version(data[0])
		, ip_type_of_serivce(data[1])
		, ip_length(data[2])
		, ip_id(data[4])
		, ip_flag_offset(data[6])
		, ip_ttl(data[8])
		, ip_proto(data[9])
		, ip_check_sum(data[10])
		, src_ip_addr(data + 12), des_ip_addr(data + 16) { }

	ip_header::ip_header(const ip_header& i)
		: header_length_and_version(i.header_length_and_version)
		, ip_type_of_serivce(i.ip_type_of_serivce)
		, ip_length(i.ip_length)
		, ip_id(i.ip_id)
		, ip_flag_offset(i.ip_flag_offset)
		, ip_ttl(i.ip_ttl)
		, ip_proto(i.ip_proto)
		, ip_check_sum(i.ip_check_sum)
		, src_ip_addr(i.src_ip_addr), des_ip_addr(i.des_ip_addr) { }

	uint8_t ip_header::get_version() const { return header_length_and_version >> 4; }
	uint8_t ip_header::get_header_length() const { return header_length_and_version & 0x0F; }
	uint16_t ip_header::get_length() const { return ntohl(ip_length); }
	uint16_t ip_header::get_id() const { return ntohl(ip_id); }
	uint8_t ip_header::get_flag() const { return ip_flag_offset >> 13; }
	uint16_t ip_header::get_frag_offset() const { return ip_flag_offset & 0x1FFF; }
	uint8_t ip_header::get_ttl() const { return ip_ttl; }
	uint8_t ip_header::get_proto() const { return ip_proto; }
	uint16_t ip_header::get_checksum() const { return ntohl(ip_check_sum); }
	ip_address ip_header::get_src_ip() const { return src_ip_addr; }
	ip_address ip_header::get_des_ip() const { return des_ip_addr; }

	std::string ip_header::to_string() const
	{
		std::ostringstream ss;

		ss << "src ip: " << src_ip_addr << std::endl
			<< "des ip" << des_ip_addr << std::endl;

		return ss.str();
	}

	PacketType ip_header::get_next_packet_type() const
	{
		switch (ip_proto)
		{
		case IP_PROTO_TCP:
			return PacketType::TCP;
		case IP_PROTO_UDP:
			return PacketType::UDP;

		default:
			break;
		}
		return PacketType::UNKNOWN;
	}

	ip_header& ip_header::operator=(const ip_header& i)
	{
		header_length_and_version = i.header_length_and_version;
		ip_type_of_serivce = i.ip_type_of_serivce;
		ip_length = i.ip_length;
		ip_id = i.ip_id;
		ip_flag_offset = i.ip_flag_offset;
		ip_ttl = i.ip_ttl;
		ip_proto = i.ip_proto;
		ip_check_sum = i.ip_check_sum;

		src_ip_addr = i.src_ip_addr;
		des_ip_addr = i.des_ip_addr;
		return *this;
	}

	bool ip_header::operator==(const ip_header& i)
	{
		return header_length_and_version == i.header_length_and_version
			&& ip_type_of_serivce == i.ip_type_of_serivce
			&& ip_length == i.ip_length
			&& ip_id == i.ip_id
			&& ip_flag_offset == i.ip_flag_offset
			&& ip_ttl == i.ip_ttl
			&& ip_proto == i.ip_proto
			&& ip_check_sum == i.ip_check_sum

			&& src_ip_addr == i.src_ip_addr
			&& des_ip_addr == i.des_ip_addr;
	}

	bool ip_header::operator!=(const ip_header& i)
	{
		return !(*this == i);
	}

	std::ostream& operator<<(std::ostream& os, const ip_header& i)
	{
		os << i.to_string();
		return os;
	}
}