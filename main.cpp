#include <iostream>

#include "ethernet.h"
#include "tcp.h"
#include "ipv4.h"
#include "DumpManager.h"

using namespace noname_packet_dump;

void usage()
{
	printf("syntax: nonameShark <interface>\n");
}

int main(int argc, char* argv[])
{
	if (argc != 2) {
		usage();
		return -1;
	}

	Dump_manager dumper(argv[1]);
	if (dumper.status == Dump_manager::STATUS::OFFLINE) {
		return -1;
	}

	while (true)
	{
		Packet packet;
		int st = dumper.get_next_packet(&packet);
		if (st == 0 || st == -1)
			std::cout << "error occur" << std::endl;
		else
			std::cout << packet.to_string();
	}

	return 0;
}
