#pragma once

#include "t_PacketDump.h"
#include "Types.h"

namespace noname_packet_dump
{
	struct tcp_header final : public header
	{
	private:
		uint16_t src_port;
		uint16_t des_port;

		uint32_t seq_num;
		uint32_t ack_num;

		uint16_t header_len_flags;

		uint16_t window;
		uint16_t check_sum;
		uint16_t urgent_ptr;

	public:
		tcp_header();
		tcp_header(const uint8_t* data);
		tcp_header(const tcp_header& t);

		//getter
		//setter

		std::string to_string() const;

		//operators
		tcp_header operator+(const tcp_header& t) = delete;
		tcp_header operator-(const tcp_header& t) = delete;
		tcp_header operator*(const tcp_header& t) = delete;
		tcp_header operator/(const tcp_header& t) = delete;
		tcp_header operator%(const tcp_header& t) = delete;
		tcp_header& operator=(const tcp_header& t);
		bool operator==(const tcp_header& t);
		bool operator!=(const tcp_header& t);

		friend std::ostream& operator<<(std::ostream& os, const tcp_header& t);
	};




}