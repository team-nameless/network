#pragma once

#include "t_PacketDump.h"
#include "Types.h"

namespace noname_packet_dump
{

#pragma pack(push, 1)
	struct ip_address final
	{
		static constexpr auto LEN = 4;

		uint8_t address[LEN];

		ip_address();
		ip_address(const uint8_t* data);
		ip_address(const ip_address& i);

		std::string to_string() const;

		ip_address operator+(const ip_address& i) = delete;
		ip_address operator-(const ip_address& i) = delete;
		ip_address operator*(const ip_address& i) = delete;
		ip_address operator/(const ip_address& i) = delete;
		ip_address operator%(const ip_address& i) = delete;
		ip_address& operator=(const ip_address& i);
		bool operator==(const ip_address& i);
		bool operator!=(const ip_address& i);

		friend std::ostream& operator<<(std::ostream& os, const ip_address& i);
	};

	struct ip_header final : public header
	{
		static constexpr auto IP_PROTO_TCP = 6;
		static constexpr auto IP_PROTO_UDP = 17;

	private:
		uint8_t		header_length_and_version;
		uint8_t		ip_type_of_serivce;
		uint16_t	ip_length;
		uint16_t	ip_id;

		uint16_t	ip_flag_offset;
		uint8_t		ip_ttl;
		uint8_t		ip_proto;
		uint16_t	ip_check_sum;

		ip_address	src_ip_addr;
		ip_address	des_ip_addr;

	public:
		ip_header();
		ip_header(const uint8_t* data);
		ip_header(const ip_header& i);

		uint8_t get_version() const;
		uint8_t get_header_length() const;
		uint16_t get_length() const;
		uint16_t get_id() const;
		uint8_t get_flag() const;
		uint16_t get_frag_offset() const;
		uint8_t get_ttl() const;
		uint8_t get_proto() const;
		uint16_t get_checksum() const;
		ip_address get_src_ip() const;
		ip_address get_des_ip() const;

		//setter will be placed here

		std::string to_string() const;
		PacketType get_next_packet_type() const;

		ip_header	operator+(const ip_header& i) = delete;
		ip_header	operator-(const ip_header& i) = delete;
		ip_header	operator*(const ip_header& i) = delete;
		ip_header	operator/(const ip_header& i) = delete;
		ip_header	operator%(const ip_header& i) = delete;
		ip_header&  operator=(const ip_header& i);
		bool		operator==(const ip_header& i);
		bool		operator!=(const ip_header& i);

		friend std::ostream& operator<<(std::ostream& os, const ip_header& i);
	};
#pragma pack(pop)
}